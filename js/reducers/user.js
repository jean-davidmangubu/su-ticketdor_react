import config from 'config';
import {
  CREATE_GAME_SESSION_COMPLETE,
  CREATE_GAME_SESSION_ERROR,
  START_GAME_SESSION_COMPLETE,
} from "../actions/gameSession";
import {
	CREATE_GAME_SESSION_ATTEMPT_COMPLETE
} from "../actions/gameSessionAttempt";

export default function user( state = null, action ) {
	if ( action.type == CREATE_GAME_SESSION_COMPLETE )
	{
		state = {
			...state,
			availableGameSessionCount: state.availableGameSessionCount + 1,
			failedCodeAttemptCount: 0
		}
	}
	if ( action.type == CREATE_GAME_SESSION_ERROR )
	{
		//let blockedUntil = (state.failedCodeAttemptCount >= config.maxUserFailedCodeAttempts - 1) ? true : null ;
		state = {
			...state,
			failedCodeAttemptCount: state.failedCodeAttemptCount + 1,
			//blockedUntil: blockedUntil
		}

		const blockedViolation = action.error.response.data.violations && action.error.response.data.violations.find( violation => violation.propertyPath == 'blockedUntil' );
		if ( blockedViolation )
		{
			state.blockedUntil = blockedViolation.message;
		}
	}
	if ( action.type == START_GAME_SESSION_COMPLETE )
	{
		state = {
			...state,
			openGameSession: action.payload.data,
			availableGameSessionCount: state.availableGameSessionCount - 1
		}
	}
	if ( action.type == CREATE_GAME_SESSION_ATTEMPT_COMPLETE )
	{
		const rewardAmount = action.payload.data.instantWin
				? action.payload.data.instantWin.rewardAmount
				: 0

		state = {
			...state,
			totalGainsAmount: state.totalGainsAmount + rewardAmount
		}

		state.openGameSession = {
			...state.openGameSession,
			remainingAttemptCount: state.openGameSession.remainingAttemptCount - 1,
			totalGainsAmount: state.openGameSession.totalGainsAmount + rewardAmount
		}

		if ( state.openGameSession && state.openGameSession.remainingAttemptCount <= 0 )
		{
			state.finishedGameSession = state.openGameSession
			state.openGameSession = null
		}
	}
	return state;
}

