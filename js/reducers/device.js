import { WINDOW_RESIZE, BYPASS_DEVICE_DETECTION } from '../actions/bypassDeviceDetection';
import bowser from 'bowser';
import compareVersions from 'compare-versions';

const device = bowser.getParser(window.navigator.userAgent);
const defaultState = {
	device,
	isRACompatible: (
		( device.isOS('ios') && compareVersions( device.getOSVersion(), '11.0.0' ) >= 0 )
		||
		( device.isOS('android') && compareVersions( device.getOSVersion(), '7.0.0' ) >= 0 )
	),
	unknownRACompatibility: (
		(
			device.isOS('android')
			&& compareVersions( device.getOSVersion(), '4.4.0' ) >= 0
			&& compareVersions( device.getOSVersion(), '7.0.0' ) < 0
		)
		||
		device.isOS('windows phone')
		||
		(
			device.isOS('windows')
			&& device.isPlatform('mobile')
		)
	),
	isMobile: device.isPlatform('mobile'),
	isIOS: device.isOS('ios'),
	isSafari: device.is('safari') && !device.is('chrome'),
	bypassDetection: false
};
console.log(defaultState);

export default function( state = defaultState, action ) {
	if (action.type == BYPASS_DEVICE_DETECTION) {
		return {
			...state,
			bypassDetection: true
		}
	}
	return state;
}
