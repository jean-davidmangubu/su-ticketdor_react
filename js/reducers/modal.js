import { SHOW_MODAL, HIDE_MODAL } from '../actions/modal';

export default function modal( state = { visible: false }, action ) {
	switch ( action.type ) {
		case SHOW_MODAL:
			return {
				visible: true,
				type: action.modalType,
				props: action.props,
				children: action.children
			};
		case HIDE_MODAL:
			return {
				visible: false,
				type: state.type
			};
		default:
			return state;
	}
}

modal.extend = ( reducer ) => {
	return ( state, action, ...rest ) => reducer( modal( state, action ), action, ...rest );
}