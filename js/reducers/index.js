import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import modal from "./modal";
import user from "./user";
import rewardsState from "./rewardsState";
import gameSessionStartup from "./gameSessionStartup";
import modalExtension from "./modalExtension";
import gameSessionAttempt from './gameSessionAttempt';
import device from './device';

export default combineReducers({
    form: formReducer,
    modal: modal.extend( modalExtension ),
    user,
    rewardsState,
    gameSessionStartup,
	gameSessionAttempt,
	device,
});