import { LOCATION_CHANGE } from "connected-react-router";
import { CREATE_GAME_SESSION_ATTEMPT_ERROR } from '../actions/gameSessionAttempt';

export default function modal( state = { visible: false }, action ) {
	switch ( action.type ) {
        case LOCATION_CHANGE:
            return {
                ...state,
                visible: false
            }
        case CREATE_GAME_SESSION_ATTEMPT_ERROR:
            if ( action.error.status != 200 )
            {
                return {
                    type: 'GAME_ERROR',
                    visible: true
                }
            }
		default:
			return state;
	}
}
