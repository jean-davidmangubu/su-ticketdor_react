import {
  START_GAME_SESSION_LOADING,
  START_GAME_SESSION_ERROR,
  START_GAME_SESSION_COMPLETE,
} from "../actions/gameSession";

const defaultState = {
    isLoading: false,
    error: null
}

export default function gameSessionStartup( state = defaultState, action ) {
    if ( action.type == START_GAME_SESSION_LOADING )
    {
        return {
            isLoading: true,
            error: null
        };
    }
    if ( action.type == START_GAME_SESSION_COMPLETE )
    {
        return {
            isLoading: false,
            error: null
        };
    }
    if ( action.type == START_GAME_SESSION_ERROR )
    {
        return {
            isLoading: false,
            error: action.error.response
        };
    }

    return state;
}