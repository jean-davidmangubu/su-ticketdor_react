import {
  CREATE_GAME_SESSION_ATTEMPT_LOADING,
  CREATE_GAME_SESSION_ATTEMPT_ERROR,
  CREATE_GAME_SESSION_ATTEMPT_COMPLETE,
} from "../actions/gameSessionAttempt";

const defaultState = {
    isLoading: false,
    error: null,
    content: null
}

export default function gameSessionAttempt( state = defaultState, action ) {
    if ( action.type == CREATE_GAME_SESSION_ATTEMPT_LOADING )
    {
        return {
            isLoading: true,
            error: null,
            content: null
        };
    }
    if ( action.type == CREATE_GAME_SESSION_ATTEMPT_COMPLETE )
    {
        return {
            isLoading: false,
            error: null,
            content: action.payload.data
        };
    }
    if ( action.type == CREATE_GAME_SESSION_ATTEMPT_ERROR )
    {
        return {
            isLoading: false,
            error: action.error.response,
            content: null
        };
    }

    return state;
}