import { createStore, applyMiddleware, compose } from "redux";
import logger from "redux-logger";
import axios from "axios";
import axiosMiddleware from "redux-axios-middleware";
import rootReducer from "./reducers";
import { connectRouter, routerMiddleware } from "connected-react-router";

export default function configureStore(preloadedState, history) {
	const client = axios.create( {
		baseURL: config.apiURL,
		responseType: 'json'
	} );

	const composeEnhancers =
	window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
	const store = createStore(
		connectRouter(history)(rootReducer),
		preloadedState,
		composeEnhancers(
			applyMiddleware(
				routerMiddleware( history ),
				axiosMiddleware( client ),
				logger
			)
		)
	);
	return store;
}
