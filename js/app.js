import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import configureStore from './store';
import { Provider } from 'react-redux';
import config from 'config';
import { ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import SystemeUTicketDOr from './containers/SystemeUTicketDOr';
import ga from './utils/ga.js';

const browserHistory = createBrowserHistory({
	basename: config.basePath,
});
const store = configureStore( window.preloadedState || {}, browserHistory );
ga( browserHistory );


ReactDOM.render(
	<Provider store={store}>
		<ConnectedRouter history={browserHistory}>
			<SystemeUTicketDOr />
		</ConnectedRouter>
	</Provider>,
	document.getElementById( "appContainer" )
);
