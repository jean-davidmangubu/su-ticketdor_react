import React from 'react';

const Rules = (props) => (
	<section className="rules">
		<h4><a href="#" onClick={() => {
            window.gtag('event', 'click_cta', {
                'event_category': 'FAQ',
                'event_label': 'Règlement',
                'value': 'Règlement'
            });

		}}>Règlement</a></h4>

	</section>
);
/*<p>
			Les champs signalés d'une astérisque sont obligatoires pour la prise en compte de votre participation au tirage au sort. Données personnelles : Vos données font l'objet d'un traitement par COOPÉRATIVE U ENSEIGNE et par votre MAGASIN U responsables de ce traitement, à des fins de gestion de votre participation. Elles ne pourront être utilisées et conservées au-delà d'une durée de 26 mois à compter de votre dernière connexion au site Magasins-u.com. Vos données peuvent être communiquées à des prestataires situés en dehors de l'Union européenne qui s'engagent à les traiter dans le respect de la réglementation européenne applicable à la protection des données à caractère personnel. Conformément à la réglementation applicable en matière de protection des données à caractère personnel, vous disposez d’un droit d’accès, de rectification, de suppression et de portabilité de vos données, ainsi que d’un droit d’opposition et de limitation à l'ensemble des données vous concernant. Vous disposez également du droit de décider du sort de vos données après votre décès. Ces droits s'exercent soit en écrivant : Coopérative U Enseigne - Direction Marketing – Service Fidélisation, Data & CRM - 20 rue d'Arcueil – CS 10043 – 94533 Rungis Cedex - soit par email : contact_donnees_personnelles@systeme-u.fr Vous pouvez à tout moment exercer votre droit de recours auprès de l’Autorité compétente en matière de protection des données personnelles (CNIL).
		</p>*/

export default Rules;