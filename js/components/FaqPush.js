import React from 'react';
import { Link } from 'react-router-dom';

export default function FaqPush(props) {
	return <section className="faq">
		<h4>Vous avez une question ?</h4>
		<p>Votre Magasin U est là pour vous !</p>
		<Link to="/faq" className="button" onClick={() => {

            window.gtag('event', 'click_cta', {
                'event_category': 'Home',
                'event_label': 'Voir les réponses',
                'value': 'Voir les réponses'
            });
		}
        }>Voir les réponses</Link>
	</section>;
}