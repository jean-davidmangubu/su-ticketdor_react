import React from 'react';
import config from 'config';
import { Link } from 'react-router-dom';

export default class GameErrorModal extends React.Component {
    render() {
        return (
            <div>
                <a href={config.loginURL} id="goOut" style={{display: 'none'}}>Retour</a>
                <img src={config.assetBaseURL + '/images/icons/error.svg'} className="icon" />
                <h4>Oups !</h4>
                <p>
                    Votre chasse s'est interrompue !<br />
                    Essayez de recharger la page.<br />
                    <strong>
                        En cas de gain,<br />
                        ceux-ci ont été sauvegardés !
                    </strong>
                </p>
                <button className="button" onClick={() => {

                    window.gtag('event', 'click_cta', {
                        'event_category': 'Game Error',
                        'event_label': 'Actualiez la page',
                        'value': 'Actualiez la page'
                    });;
                    window.location.reload()
                }}>Actualisez la page</button>
                <br />
                {/*<Link className="quit" to="/">Terminez la partie</Link>*/}
                <a className="quit" onClick={() => {
                    window.gtag('event', 'click_cta', {
                        'event_category': 'Game Error',
                        'event_label': 'Terminez la partie',
                        'value': 'Terminez la partie'
                    });
                    document.getElementById('goOut').click();}}>Terminez la partie</a>

            </div>

        )
    }
}