import React from 'react';
import Slider from "react-slick";

const sliderSettings = {
	dots: false,
	infinite: false,
	swipe: false,
	speed: 500,
	slidesToShow: 3,
	slidesToScroll: 3,
	autoplay: false,
	responsive: [
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				dots: true,
				swipe: true,
			}
		}
	]
}

const Tutorial = props => (
	<section className="tutorial">
		<h4>Comment jouer ?</h4>
		<Slider {...sliderSettings} >
			<article>
				<img src={config.assetBaseURL + '/images/icons/howtoplay-icon-1.png'} className="icon" />
				<p>Sur présentation de votre <strong>Carte U</strong>, recevez <strong>jusqu’à 2 tickets d’Or :</strong></p>
				<p><strong>1 TICKET</strong> à partir de 25€ d’achats</p>
				<p><strong>1 TICKET</strong> pour l’achat d’au moins 1 produit des marques partenaires.</p>
			</article>
			<article>
				<img src={config.assetBaseURL + '/images/icons/howtoplay-icon-2.png'} className="icon"/>
				<p><strong>Grattez votre ticket</strong> et découvrez si vous avez gagné des € Carte U.</p>
				<p>Vous n’avez pas gagné ? Une <strong>seconde chance</strong> vous est offerte avec le <strong>code de jeu</strong> découvert.</p>
			</article>
			<article>
				<img src={config.assetBaseURL + '/images/icons/howtoplay-icon-3.png'} className="icon"/>
				<p>Une fois votre code obtenu, <strong>rendez-vous dans votre magasin</strong> pour jouer avec un smartphone compatible</p>
			</article>
		</Slider>
	</section>
);
export default Tutorial;