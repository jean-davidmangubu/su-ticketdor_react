import React from 'react';
import { Link } from 'react-router-dom';

export default class AttemptResultModal extends React.Component {
    constructor( ...args ) {
        super(...args);
        this.showGameSessionEndModal = this.showGameSessionEndModal.bind( this );
    }

    render() {
        const { success, remainingAttempts } = this.props;
        return (
            <section className="attempt-result">
                {success ? this.renderSuccess() : this.renderFailure()}
                {remainingAttempts > 0 ? this.renderButtons() : this.renderGameOverButton()}

                <a href={config.loginURL} id="goOut" onClick={() => {
                    window.gtag('event', 'click_cta', {
                        'event_category': 'AttemptResultModal',
                        'event_label': 'Retour',
                        'value': 'Retour'
                    });
                }} style={{display: 'none'}}>Retour</a>
            </section>
        )
    }

    renderSuccess() {
		const { gainAmount } = this.props;
        return (
            <div className="success">
				<img src={config.assetBaseURL + '/images/icons/win.svg'} className="icon" />
                <h4>Félicitation,<br />
                Vous remportez</h4>
                <strong className="big">{gainAmount}€ Carte U</strong>
            </div>
        )
    }

    renderFailure() {
        return (
            <div className="failure">
				<img src={config.assetBaseURL + '/images/icons/lost.svg'} className="icon" />
                <h4>Le ticket d'Or<br/>vous a échappé !</h4>
            </div>
        )
    }

    renderButtons() {
		const { remainingAttempts } = this.props;
        return (
            <React.Fragment>
				<p>Encore {remainingAttempts} QR Code{remainingAttempts > 1 && 's'} à scanner.</p>
                <button className="button" onClick={() => {
                    this.props.hideModal();
                    window.gtag('event', 'click_cta', {
                        'event_category': 'AttemptResultModal',
                        'event_label': 'Continuez',
                        'value': 'Continuez'
                    });
                }}>Continuez</button>
				<br/>
                {/*<Link className="quit" to="/">Quittez le jeu</Link>*/}
                <a  className="quit"  onClick={() => {
                    window.gtag('event', 'click_cta', {
                        'event_category': 'AttemptResultModal',
                        'event_label': 'Quittez le jeu',
                        'value': 'Quittez le jeu'
                    });
                    document.getElementById('goOut').click();
                }}>Quittez le jeu</a>
            </React.Fragment>
        )
    }

    renderGameOverButton() {
            return <button className="button" onClick={this.showGameSessionEndModal}>Terminez la partie</button>;
    }

    showGameSessionEndModal() {
        const { success, gainAmount, totalGainsAmount, remainingAttempts } = this.props;
        const notclosable = true;
        window.gtag('event', 'click_cta', {
            'event_category': 'AttemptResultModal',
            'event_label': 'Terminez la partie',
            'value': 'Terminez la partie'
        });

        this.props.showModal( 'GAME_SESSION_END', {
            props: {
                notclosable,
                success,
                gainAmount,
                totalGainsAmount,
                remainingAttempts
            }
        } );
    }
}