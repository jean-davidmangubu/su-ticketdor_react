import React from 'react';
import { reset, reduxForm, Field, SubmissionError } from 'redux-form';
import classNames from 'classnames';
import { createGameSession } from '../actions/gameSession';
import config from 'config';

class CodeForm extends React.Component
{
	constructor(props){
		super(props);
		this.state = {
		    codeValue: ''
        };

        this.handleChange = this.handleChange.bind(this);

	}
    handleChange = (value) =>{
        this.setState({codeValue: value});
    }

    componentDidUpdate(){
	    var THIS = this;
	    if(this.props.submitSucceeded ){


	        setTimeout(function(){
                THIS.props.submitSucceeded=false;
                THIS.handleChange('');
                THIS.setState({codeValue: '', submitSucceeded: false});
                /** clear **/
                THIS.props.change("code", null);
            }, 2000);
        }

    }
    render()
    {
        const { handleSubmit, submitting, error, user, gameSessionStartup, isMobile } = this.props;

        const userBlockedUntil = user.blockedUntil && new Date( user.blockedUntil );
		const userIsBlocked = userBlockedUntil && userBlockedUntil > (new Date());
		const isLoading = submitting || gameSessionStartup.isLoading;
        const isDisabled = submitting || userIsBlocked || gameSessionStartup.isLoading;

        if(!userIsBlocked && user.failedCodeAttemptCount >= config.maxUserFailedCodeAttempts ){
            window.location.reload();
        }

        console.log(user);

        console.log(userIsBlocked, 'blocked ?');
        return (

            <form>
                <p className="saveSuccess" style={{ display:  ( !error && this.props.submitSucceeded && this.state.codeValue != "" ? 'block' : 'none' )  }}>
                    <img src={config.assetBaseURL + '/images/icons/check.svg'} className="icon"/>
                    <h4>Votre code<br/>a bien été enregistré !</h4>
                </p>
                <div id="form"  style={{ display:  ( this.state.codeValue == "" || error || !this.props.submitSucceeded ? 'block' : 'none' )  }}>
                <h4>
					Vous avez un code ?
					<br/>
					{
						isMobile ?
						'Jouez dès maintenant ou sauvegardez-le pour plus tard !' :
						'Sauvegardez-le pour jouer plus tard en magasin, depuis votre smartphone.'
					}
				</h4>
                <div style={{display: ( userIsBlocked ? 'none' : 'block' ) }}>
                <Field name="code" id="mycode" value={this.codeValue}  component="input" type="text"   onChange={(e) => this.handleChange(e.target.value) }  placeholder="Tapez votre code ici"/>
                </div>
                {!userIsBlocked && error && <p className="error" dangerouslySetInnerHTML={{__html: error }}></p>}
                {!userIsBlocked && user.failedCodeAttemptCount >= config.maxUserFailedCodeAttempts - 1 && <p className="error">Dernière tentative <strong>avant blocage de 24h</strong></p>}
				{userIsBlocked && <p className="error">Vous avez réalisé trop de tentatives d'ajout de code infructueuses. Votre compte est donc <strong>bloqué jusq'au {userBlockedUntil.toLocaleString()}</strong></p>}


                <p>Pour consulter le règlement du jeu, <a href="#" onClick={() => {

                    window.gtag('event', 'click_cta', {
                        'event_category': 'Home',
                        'event_label': 'Règlement',
                        'value': 'Règlement'
                    });
                }}>cliquez ici</a></p>

				{
                    this.state.codeValue != "" ?
					 isMobile &&  ! user.availableGameSessionCount ?
					<React.Fragment>
						<button
							disabled={isDisabled}
							className={classNames({
								submit: true,
								button: true,
								isLoading
							})}
							onClick={handleSubmit( ( values, dispatch, props ) => submit( true, values, dispatch, props, 'Jouez' ) )}
						>
							Jouez
						</button>


						<a href="#" className={classNames({
							saveCode: true,
							isLoading
						})} disabled={isDisabled} onClick={ handleSubmit((values, dispatch, props) => submit(false, values, dispatch, props, 'Sauvegardez ce code') )  }>Sauvegardez ce code</a>
					</React.Fragment> :
					<a href="#" className={classNames({
						//saveCode: true,
						button: true,
						isLoading
					})} disabled={isDisabled} onClick={ handleSubmit((values, dispatch, props) => submit(false, values, dispatch, props, 'Sauvegardez ce code'))}>Sauvegardez ce code</a>

                        : ''
				}
                </div>
            </form>
        )
	}

}

function submit( startGameSession, values, dispatch, props, log )
{
    window.gtag('event', 'click_cta', {
        'event_category': 'Home',
        'event_label': log,
        'value': log
    });

    return dispatch( createGameSession( values.code ) ).then( action => {
        if ( action.error )
        {
            let error = 'Une erreur est survenue, veuillez réessayer plus tard.';
            if ( action.error.response.status == 404)
            {
                error = 'Le code que vous avez entré est invalide<br/>Veuillez réessayer.';
            }
            else if ( action.error.response.status == 400 )
            {
                error = 'Oh, ce code <strong>a déjà été utilisé !</strong><br />Veuillez entrer un nouveau code valide.';

            }
            throw new SubmissionError({
                _error: error
            });
        }
        else if ( startGameSession )
        {
            reset();
            props.startGameSession();
        }
    } );
}

export default reduxForm( {
    form: 'code'
} )( CodeForm );
