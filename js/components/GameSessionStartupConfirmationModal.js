import React from 'react';
import { Link } from 'react-router-dom';
import config from 'config';

export default class GameSessionStartupConfirmationModal extends React.Component {
    render() {
        return (
            <section>
				<img src={config.assetBaseURL + '/images/icons/game-closed.svg'} className="icon"/>
                <h4>Un peu de patience</h4>
				<br/>
				<p>La chasse aux Tickets d'Or est uniquement ouverte aux horaires d'ouverture de votre Magasin U</p>
                {/* <Link to="/game-tutorial" className="button">Jouez</Link><br/> */}
                <a href="#" className="button" onClick={e=>{window.gtag('event', 'click_cta', {
                    'event_category': 'Home',
                    'event_label': 'Jouez plus tard',
                    'value': 'Jouez plus tard'
                });
                e.preventDefault();this.props.hideModal()}}>Jouez plus tard</a>
            </section>
        )
    }
}