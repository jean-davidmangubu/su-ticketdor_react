import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames'
import { showModal, hideModal } from '../actions/modal';

class Modal extends React.Component {
	static propTypes = {
		modal: PropTypes.object,
		components: PropTypes.object
	}

	render() {
		const ModalComponent = this.props.components[ this.props.modal.type ];
		var closable = true;
		if( ['GAME_CLOSE_CONFIRMATION', 'ATTEMPT_RESULT', 'GAME_SESSION_END'].indexOf(this.props.modal.type) > -1 ) closable = false;

		const children = this.props.modal.children || null;
		const props = this.props.modal.props || {};
		const visible = (closable ) ? 'block': 'none';
		const mystyle = {display: visible};
		return (
			<div className={classNames({
					[ 'overlay' ]: true,
					[ 'visible' ]: this.props.modal.visible
				})} onClick={ closable ? this.props.hideModal : () => {}  }>
				<div className="modal" onClick={event=>event.stopPropagation()}>
					<button className="close" style={mystyle}  onClick={this.props.hideModal }></button>
					<div className="content">
						{ModalComponent && <ModalComponent showModal={this.props.showModal} hideModal={this.props.hideModal} {...props}>{children}</ModalComponent>}
					</div>
				</div>
			</div>
		);
	}
    componentDidMount() {
        this.interval = setInterval(() => this.setState({ modalVisible: this.props.modal.visible }), 500);
    }
}

function mapStateToProps( state ) {
	return {
		modal: state.modal
	}
}

function mapDispatchToProps( dispatch ) {
	return bindActionCreators( {
		showModal,
		hideModal,
	}, dispatch );
}

export default connect( mapStateToProps, mapDispatchToProps )( Modal );