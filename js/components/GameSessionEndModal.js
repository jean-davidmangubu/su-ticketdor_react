import React from 'react';
import { Link } from 'react-router-dom';
import { startGameSession } from '../actions/gameSession';
import { connect } from 'react-redux';
import config from 'config';
import {bindActionCreators} from "redux";
import {push} from "connected-react-router";
import classNames from "classnames";

class GameSessionEndModal extends React.Component {
    constructor(...args) {
        super(...args);
        const { isLoading } = this.props;
        this.handleFacebookButtonClick = this.handleFacebookButtonClick.bind( this );
        this.handleTwitterButtonClick = this.handleTwitterButtonClick.bind( this );
        this.showGameSessionEndModal = this.showGameSessionEndModal.bind( this );
        this.newSession = this.newSession.bind(this);
    }

    render() {
        var windowHref = localStorage.getItem('windowHref') || '';

        return (
            <section>
                {this.props.totalGainsAmount > 0 ? this.renderSuccess() : this.renderFailure()}
                <a href={config.loginURL} id="goOut" style={{display: 'none'}}>Retour</a>
                <a href={'/game'+windowHref} id="refresh" style={{display: 'none'}}>Refresh</a>
            </section>
        )
    }

    renderSuccess() {
        const { isLoading } = this.props;
        return (
            <div className="success">
				<img src={config.assetBaseURL + '/images/icons/win.svg'} className="icon" />
                <h4>{this.props.user.firstname}<br/>lors de cette partie<br/>vous avez remporté</h4>
                <strong className="big">
                    {this.props.totalGainsAmount}€ Carte U
                </strong>
                <p>
                    L'ensemble de vos gains seront automatiquement crédités sur votre Carte U à la fin de l'opération,
                    <strong> à partir du 7 Décembre 2018</strong>
                </p>
                { this.props.user.availableGameSessionCount > 0
                    ? <div>
                        {/*<Link className="button" to="/game-tutorial">Rejouez</Link>*/}
                        <button className={classNames({
                            button: true,
                            isLoading: isLoading
                        })}
                                onClick={() => {
                                    window.gtag('event', 'click_cta', {
                                        'event_category': 'Game',
                                        'event_label': 'Rejouez',
                                        'value': 'Rejouez'
                                    });;
                                    this.newSession();  } } disabled={isLoading}>Rejouez</button>

                        <br/>

                        {/*<Link className="quit" to="/">Quittez le jeu</Link>*/}
                        <a className="quit"  onClick={() => {  window.gtag('event', 'click_cta', {
                            'event_category': 'Game',
                            'event_label': 'Quittez le jeu',
                            'value': 'Quittez le jeu'
                        });
                        document.getElementById('goOut').click();}}>Quittez le jeu</a>

                    </div>
                    : <div>
                        {/*<Link className="quit" to="/">Quittez le jeu</Link>*/}
                        <a className="button end-game" onClick={() => {window.gtag('event', 'click_cta', {
                            'event_category': 'Game',
                            'event_label': 'Quittez le jeu',
                            'value': 'Quittez le jeu'
                        });
                        document.getElementById('goOut').click();}}>Quittez le jeu</a>
                    </div>
                }
                <section class="share">
                    <p>Partagez le résultat avec vos amis !</p>
					<ul>
						<li className="facebook">
							<button onClick={(e) => {
                                window.gtag('event', 'click_cta', {
                                    'event_category': 'Game',
                                    'event_label': 'Partage FB',
                                    'value': 'Partage FB'
                                });

							    this.handleFacebookButtonClick(e);
                            }}><img src={config.assetBaseURL + '/images/icons/facebook.svg'} className="icon" /></button>
						</li>
						<li className="twitter">
							<button onClick={(e) => {
                                window.gtag('event', 'click_cta', {
                                    'event_category': 'Game',
                                    'event_label': 'Partage Twitter',
                                    'value': 'Partage Twitter'
                                });
							    this.handleTwitterButtonClick(e);
                            }}><img src={config.assetBaseURL + '/images/icons/twitter.svg'} className="icon" /></button>
						</li>
						<li className="email">
							<a onClick={() => {
                                window.gtag('event', 'click_cta', {
                                    'event_category': 'Game',
                                    'event_label': 'Partage mail',
                                    'value': 'Partage mail'
                                });
                            }} href={`mailto:?subject=${encodeURIComponent('Grand jeu Ticket d\'or')}&body=${encodeURIComponent(this.getShareMessage() + ' ' + config.shareURL )}`}><img src={config.assetBaseURL + '/images/icons/email.svg'} className="icon" /></a>
						</li>
					</ul>
				</section>
            </div>
        );
    }

    renderFailure() {
        const { isLoading } = this.props;
        return (
			<div className="failure">
				<img src={config.assetBaseURL + '/images/icons/lost.svg'} className="icon" />
                <h4>Vous avez perdu</h4>
                <p>
                    Repartez à la chasse et tentez votre chance avec de nouveaux codes de participation au Ticket d'Or !
                </p>
                { this.props.user.availableGameSessionCount > 0
                    ? <div>
                        {/* <Link className="button" to="/game-tutorial">Utilisez un code sauvegardé</Link>*/}
                        <button className={classNames({
                            button: true,
                            isLoading: isLoading
                        })}  onClick={() => {this.newSession();  } }  disabled={isLoading}>Utilisez un code sauvegardé</button>
						<br/>
                        {/*<Link className="quit" to="/">Quittez le jeu</Link>*/}
                        <a className="quit"  onClick={() => {document.getElementById('goOut').click();}}>Quittez le jeu</a>
                    </div>
                    : <div>
                        {/*<Link className="quit" to="/">Terminez la partie</Link>*/}
                        <a className="button end-game" onClick={() => {document.getElementById('goOut').click()} }>Terminez la partie</a>
                    </div>
                }
            </div>
        );
    }

    newSession(){
        if(typeof this.props.startGameSession != "undefined")
            this.props.startGameSession().then( () => {
                document.getElementById('refresh').click();
            } );

    }

    handleFacebookButtonClick(event) {
		event.preventDefault();
        /*window.FB.ui({
            method: 'feed',
            link: config.shareURL,
            caption: this.getShareMessage(),
        });*/
        window.FB.ui({
            method: 'share_open_graph',
            action_type: 'og.shares',
            display: 'popup',
            action_properties: JSON.stringify({
                object: {
                    'og:url': config.shareURL,
                    'og:title': config.og_title,
                    'og:description': config.og_description,
                    'og:image' : config.og_image
                    //'og:image': 'https://systemeu2018.greenshift.eu/images/home/tickets.png'
                }
            })
        }, function(response) {
            // Action after response
        });

    }
    showGameSessionEndModal() {
        const { success, gainAmount, totalGainsAmount, remainingAttempts } = this.props;
        const notclosable = true;
        this.props.showModal( 'GAME_SESSION_END', {
            props: {
                notclosable,
                success,
                gainAmount,
                totalGainsAmount,
                remainingAttempts
            }
        } );
    }
    handleTwitterButtonClick(event) {
		event.preventDefault();
        window.open(`https://twitter.com/intent/tweet?text=${encodeURIComponent(this.getShareMessage())}&url=${encodeURIComponent(config.shareURL)}`, "twitter", "width=640,height=320");
    }

    getShareMessage() {
        return `Venez vite dans votre Magasin U du mardi 6 novembre au dimanche 2 décembre 2018, pour participer au Grand Jeu Le Ticket d'Or et tentez de gagner jusqu'à 250€ Carte U ! #ChasseAuxTicketsd'Or`;
    }
}

function mapStateToProps( state ) {
    return {
        user: state.user,
        isLoading: state.gameSessionStartup.isLoading
    };
}


function mapDispatchToProps( dispatch )
{
    return bindActionCreators( {
        startGameSession
    }, dispatch );
}
export default connect( mapStateToProps, mapDispatchToProps )( GameSessionEndModal );
