import React from 'react';
import { Link } from 'react-router-dom';

export default class GameSessionStartModal extends React.Component {
    render() {
		const { remainingAttempts, remainingTime } = this.props,
			hours = Math.floor( remainingTime / 3600000 ),
			minutes = Math.floor( remainingTime % 3600000 / 60000 );

        return (
            <section>
				<img src={config.assetBaseURL + '/images/icons/scan.svg'} className="icon" />
                <h4>La chasse commence !</h4>
				<br/>
                <p>Vous avez {hours}h{minutes} pour scanner<br/><strong>{remainingAttempts} QR Code{remainingAttempts > 1 && 's'}.</strong></p>
				<br/>
                <p>Bonne chance !</p>
            </section>
        )
    }
}