import React from 'react';
import config from 'config';
import {Link} from 'react-router-dom';

export default class GameCloseConfirmationModal extends React.Component {

    render() {
        return (
            <div>
				<img src={config.assetBaseURL + '/images/icons/warn.svg'} className="icon" />
                <h4>Vous partez déjà ?</h4>
                <p>
				En cas de gains, ceux-ci sont sauvegardés même si vous terminez votre partie maintenant.
                </p>
                {/*<Link className="button end-game" to="/">Terminez la partie</Link>*/}
                <a className="button end-game" onClick={() => {

                    window.gtag('event', 'click_cta', {
                        'event_category': 'Game',
                        'event_label': 'Terminez la partie',
                        'value': 'Terminez la partie'
                    });
                    document.getElementById('goOut').click();
                }}>Terminez la partie</a>
				<br/>
				<a href="#" onClick={e=>{

                    window.gtag('event', 'click_cta', {
                        'event_category': 'Game',
                        'event_label': 'Continuez à jouer',
                        'value': 'Continuez à jouer'
                    });
                    e.preventDefault();this.props.hideModal()}} className="quit">Continuez à jouer</a>
                {/* TODO ajouter les boutons de partage */}
            </div>
        )
    }
}