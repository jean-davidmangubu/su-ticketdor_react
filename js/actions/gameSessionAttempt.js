export const CREATE_GAME_SESSION_ATTEMPT_LOADING = 'CREATE_GAME_SESSION_ATTEMPT_LOADING';
export const CREATE_GAME_SESSION_ATTEMPT_COMPLETE = 'CREATE_GAME_SESSION_ATTEMPT_COMPLETE';
export const CREATE_GAME_SESSION_ATTEMPT_ERROR = 'CREATE_GAME_SESSION_ATTEMPT_ERROR';

export function createGameSessionAttempt()
{
    return {
        types: [ CREATE_GAME_SESSION_ATTEMPT_LOADING, CREATE_GAME_SESSION_ATTEMPT_COMPLETE, CREATE_GAME_SESSION_ATTEMPT_ERROR ],
        payload: {
            request: {
                method: 'post',
                url: '/game_session_attempts',
                data: {
                    
                }
            }
        }
    };
}