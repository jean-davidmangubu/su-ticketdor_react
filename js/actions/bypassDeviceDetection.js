export const BYPASS_DEVICE_DETECTION = 'BYPASS_DEVICE_DETECTION';

export function bypassDeviceDetection() {
	return { type: BYPASS_DEVICE_DETECTION }
}