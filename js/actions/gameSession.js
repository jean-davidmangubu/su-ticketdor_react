export const CREATE_GAME_SESSION_LOADING = 'CREATE_GAME_SESSION_LOADING';
export const CREATE_GAME_SESSION_COMPLETE = 'CREATE_GAME_SESSION_COMPLETE';
export const CREATE_GAME_SESSION_ERROR = 'CREATE_GAME_SESSION_ERROR';

export function createGameSession( code )
{
    return {
        types: [ CREATE_GAME_SESSION_LOADING, CREATE_GAME_SESSION_COMPLETE, CREATE_GAME_SESSION_ERROR ],
        payload: {
            request: {
                method: 'post',
                url: '/game_sessions',
                //url: 'test_game_sessions_save',
                data: {
                    codeId: code
                }
            }
        }
    };
}

export const START_GAME_SESSION_LOADING = 'START_GAME_SESSION_LOADING';
export const START_GAME_SESSION_COMPLETE = 'START_GAME_SESSION_COMPLETE';
export const START_GAME_SESSION_ERROR = 'START_GAME_SESSION_ERROR';

export function startGameSession()
{
    return {
        types: [ START_GAME_SESSION_LOADING, START_GAME_SESSION_COMPLETE, START_GAME_SESSION_ERROR ],
        payload: {
            request: {
                method: 'put',
                url: '/game_sessions/start'
            }
        }
    }
}