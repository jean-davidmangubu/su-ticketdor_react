export default function ga( history ) {
	history.listen( location => window.gtag('config', window.GA_TRACKING_ID, {
		page_path: location.pathname
	} ) );
}