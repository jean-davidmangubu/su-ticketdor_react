import React from 'react';

export default class ResizeableComponent extends React.Component {
	iframeResizeTimeout = null;

	componentDidMount() {
		this.triggerIframeResize();
	}
	componentDidUpdate() {
		this.triggerIframeResize();
	}

	triggerIframeResize( height ){
		clearTimeout(this.iframeResizeTimeout);
		this.iframeResizeTimeout = setTimeout(()=>{
			this.postIframeResize( height );
			if (!height) {
				this.iframeResizeTimeout = setTimeout( ()=>{
					this.postIframeResize( height );
				}, 300);
			}
		}, 50);
	}
	postIframeResize( height ) {
		window.parent.postMessage( {
			type: 'resizeIframe',
			height: height || document.querySelector( '#appContainer' ).scrollHeight + 30
		}, '*' );
	}

	render(){
		throw new Error('ResizableComponent::render() should be overriden');
	}
}