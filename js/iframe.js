const eventMethod = window.addEventListener ? 'addEventListener' : 'attachEvent';
const eventer = window[ eventMethod ];
const messageEvent = eventMethod === 'attachEvent' ? 'onmessage' : 'message';
const iframeSelector = '#iframe_generique';
function requestFullscreen()
{
    const iframe = document.querySelector( iframeSelector )
    if ( iframe.requestFullscreen ) { // W3C API
        iframe.requestFullscreen();
    } else if ( iframe.mozRequestFullScreen ) { // Mozilla current API
        iframe.mozRequestFullScreen();
    } else if ( iframe.webkitRequestFullScreen ) { // Webkit current API
        iframe.webkitRequestFullScreen();
    }
}

function exitFullscreen() {
    if (document.exitFullscreen) { // W3C API
        document.exitFullscreen();
    } else if (document.mozExitFullscreen) { // Mozilla current API
        document.mozExitFullscreen();
    } else if (document.webkitExitFullscreen) { // Webkit current API
        document.webkitExitFullscreen();
    }
}

function resizeIframe(height) {
	const iframe = document.querySelector( iframeSelector );
	height = Math.max( height, 300 );
	const style = `height:${height+50}px`;
	iframe.setAttribute( 'style', style );
	console.log( iframe.getAttribute( 'style' ) );
}

eventer( messageEvent, function( e ) {
    const data = e.data || e.message;
    if ( data.type == 'redirect' )
    {
        document.location.href = data.url;
    }
    else if ( data.type == 'requestFullscreen' )
    {
        requestFullscreen();
    }
    else if ( data.type == 'exitFullscreen' )
    {
		exitFullscreen();
    }
	else if ( data.type == 'resizeIframe' )
	{
		resizeIframe( data.height );
	}
});
