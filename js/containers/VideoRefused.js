import React from 'react';
import { Link } from 'react-router-dom';
import config from 'config';

const VideoRefused = (props) => (
	<section>
		<p className="welcome-message">Partez à l'aventure <br/>et trouvez les Tickets d'Or !</p>
		<p className="intro">
			<img src={config.assetBaseURL + '/images/home/tickets.png'} className="tickets" style={{width: 270, marginTop: 20, marginBottom: 30}} />
			<br/>
			<strong>Nous n'avons pas l'autorisation <br/>pour accéder à votre caméra.<br/>
			Vous ne pouvez donc pas rejoindre la <br/>chasse aux Tickets d'Or !</strong>
		</p>
		<br/>
		<p>Rendez-vous dans vos réglages pour<br/> autoriser l'accès à la caméra</p>
		<Link to="/game" className="button inverted" onClick={() => {
            window.gtag('event', 'click_cta', {
                'event_category': 'Video Refused',
                'event_label': 'Réessayer',
                'value': 'Réessayer'
            });

		}}>Réessayer</Link>
	</section>
);
export default VideoRefused;