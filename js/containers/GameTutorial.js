import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { startGameSession } from '../actions/gameSession';
import config from 'config';
import { push } from 'connected-react-router';
import Slider from 'react-slick';
import classNames from 'classnames';

class GameTutorial extends React.Component {
	slider = null;

	constructor( ...args ) {
        super( ...args );
        this.handleStartButtonClick = this.handleStartButtonClick.bind( this );
        this.handleNextButtonClick = this.handleNextButtonClick.bind( this );

        if(this.props.user.gameLaunchedSessionCount > 0){

            if(typeof this.props.startGameSession != "undefined")
                this.props.startGameSession().then( () => {
                    /*var windowHref = localStorage.getItem('windowHref') || '';
                    top.location.href = '/game'+windowHref;*/
						goGame();
                } );
        }
	}

	render() {
		const sliderSettings = {
			dots: true,
			infinite: false,
			swipe: false,
			arrows: false,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: false,
		};
		const {isLoading} = this.props;

		if(this.props.user.gameLaunchedSessionCount) {
            return '';
        }
		else
		return (
			<div className="game-tutorial">
                <section>
                    <h4>Comment ça marche ?</h4>
                    <Slider {...sliderSettings} ref={ e => this.slider = e }>
						<article>
							<img src={config.assetBaseURL + '/images/icons/game-tutorial-icon-1.png'} className="icon" />
							<p>Rendez-vous<strong> dans les rayons
                            de votre Magasin U</strong> avec votre smartphone</p>
					    	<button className="button" onClick={() =>  {
                                window.gtag('event', 'click_cta', {
                                    'event_category': 'Tutorial',
                                    'event_label': 'Suivant',
                                    'value': 'Suivant'
                                });
					    		this.handleNextButtonClick();
                            } }>Suivant</button>
						</article>
						<article>
							<img src={config.assetBaseURL + '/images/icons/game-tutorial-icon-2.png'} className="icon"/>
							<p><strong>Chassez et scannez les QR Codes</strong> dans les rayons avec votre caméra depuis <strong>leticketdor.com</strong></p>
					    	<button className="button" onClick={() => {
                                window.gtag('event', 'click_cta', {
                                    'event_category': 'Tutorial',
                                    'event_label': 'Suivant',
                                    'value': 'Suivant'
                                });
					    		this.handleNextButtonClick();

                            } }>Suivant</button>
						</article>
						<article>
							<img src={config.assetBaseURL + '/images/icons/game-tutorial-icon-3.png'} className="icon"/>
							<p>Dès que la chasse commence, <strong>vous avez 4 heures</strong> pour scanner jusqu’à 5 QR Codes et <strong>tentez de gagner des € Carte U</strong></p>
                            <button  className={classNames({
								button: true,
								isLoading
							})} disabled={isLoading} onClick={() => {
                                window.gtag('event', 'click_cta', {
                                    'event_category': 'Tutorial',
                                    'event_label': 'Commencez la chasse',
                                    'value': 'Commencez la chasse'
                                });
								this.handleStartButtonClick();
                            }}>Commencez la chasse</button>
						</article>
					</Slider>
                </section>
    		</div>
		);
	}

	handleNextButtonClick() {
		if ( this.slider ) {
			this.slider.slickNext();
		}
	}

    handleStartButtonClick() {

        if(typeof this.props.startGameSession != "undefined")
            this.props.startGameSession().then( () => {
            	//this.props.push( '/game' );

               goGame();
            } );
/*
		if (window != top){
            try
            {
                var oldHref = config.loginURL;
                history.pushState({}, 'Super U', oldHref);
            }
            catch (e) {

            }

            var windowHref = localStorage.getItem('windowHref') || '';

            top.location.href = '/game'+windowHref;
		}*/

		/*window.parent.postMessage({
			type: 'requestFullscreen'
		}, '*');*/

    }
}
function goGame(){
    if (window != top){
        try
        {
            var oldHref = config.loginURL;
            history.pushState({}, 'Super U', oldHref);
        }
        catch (e) {

        }

        var windowHref = localStorage.getItem('windowHref') || '';

        top.location.href = '/game'+windowHref;
    }

}
function mapStateToProps( state )
{
	return {
	    windowHref: state.windowHref,
		user: state.user,
        isLoading: state.gameSessionStartup.isLoading
	};
}

function mapDispatchToProps( dispatch )
{
	return bindActionCreators( {
        startGameSession,
        push
	}, dispatch );
}

export default connect( mapStateToProps, mapDispatchToProps )( GameTutorial );