import React from 'react';
import {Link} from 'react-router-dom';
import classNames from 'classnames';
import config from 'config';
import ResizeableComponent from '../utils/ResizableComponent';

class QuestionRenderer extends ResizeableComponent {
	state = {
		opened: false,
	}

	render(){
		const {label, content} = this.props.question,
			{opened} = this.state;

		return <React.Fragment>
			<h4 className={classNames({
					question: true,
					opened
				})} onClick={ ()=> this.setState({ opened:!opened })}>
				{label}
			</h4>
			{ opened &&
				<div className="answer">{content}</div>	}
		</React.Fragment>;
	}
}

const questions = {
	sections: [
		{ title: 'Participation au jeu « La Chasse aux Tickets d’Or »', questions: [
			{ label: 'Qu’est-ce que « La Chasse aux Tickets d’Or » ?',
			  content: <React.Fragment>
				  <p>La Chasse aux Tickets d’Or invite les participants munis d’un ticket à gratter <a href="https://www.magasins-u.com/leticketdor" target="_blank">«Le Ticket d’Or»</a> mentionnant un code de jeu, à chasser les tickets d’or dans leurs Magasin U.</p>
					<p>Pour chaque code de jeu obtenu, vous serez invité à réaliser une partie de jeu en scannant, depuis votre smartphone jusqu’à 5 QR Codes dissimulés dans les rayons de votre magasin U.</p>
					<p>Chaque QR Code scanné lancera une animation en réalité augmentée et vous permettant de découvrir immédiatement si vous avez remporté des € Carte U.</p>
					<p>Pour découvrir comment obtenir un ticket à gratter « Le Ticket d’Or », <a href="https://www.magasins-u.com/leticketdor">cliquez-ici</a></p>
			  </React.Fragment> },
			{ label: 'Comment participer au jeu ?',
			  content: <React.Fragment>
				  <p>Vous avez obtenu un ticket à gratter « Le Ticket d’Or » mentionnant un code de jeu ? Félicitations, vous bénéficiez d’une deuxième chance de remporter des € Carte U avec notre jeu « La Chasse aux Tickets d’Or ».</p>
					<p>
						Pour participer au jeu dans les rayons de votre magasin U, rien de plus simple :
						<ul>
							<li>Rendez-vous sur <a href="https://www.magasins-u.com/leticketdor/jeu-digital" target="_blank">leticketdor.com</a> depuis votre smartphone et cliquez sur le bouton de participation prévu à cet effet.</li>
							<li>Si ce n’est pas déjà fait, connectez-vous à votre Espace U. Si vous n’avez pas de compte Espace U, vous serez invité à en créer un et à y associer votre Carte U.</li>
							<li>Renseignez votre code de participation et lancez une partie ou enregistrez votre code pour jouer ultérieurement.</li>
							<li>Lorsque la partie commence, vous disposez de 4 heures pour scanner jusqu’à 5 QR Codes dans les rayons de votre magasin U. Lorsque un QR Code est scanné, une animation en réalité augmentée apparaît et vous informe si vous avez gagné des € Carte U.</li>
						</ul>
					</p>
			  </React.Fragment> },
			{ label: 'Comment obtenir un code de jeu ?',
			  content: <React.Fragment>
					<p>Les codes de jeu sont disponibles sur certains tickets à gratter « Le Ticket d’Or » Pour découvrir comment obtenir un Ticket d’Or, veuillez consulter la question «Comment participer au Grand Jeu Le Ticket d’Or ?» de cette page.</p>
			  </React.Fragment> },
			{ label: 'Où trouver les QR codes dans mon magasin u ?',
			  content: <React.Fragment>
					<p>Les QR Codes sont présents dans les rayons des Magasins U participants et sont apposés sur certaines affiches relayant des produits.</p>
					<p>Ces QR Codes sont matérialisés par un code barre en 2D constitué de blocs noir dans un carré fond blanc accompagné par le message « flashez cette image ».</p>
					<p><img src={config.assetBaseURL + '/images/faq-poster.jpg'} /><br/><em>Exemple d’affiche. Visuel non contractuel à titre d’information, QR Code et  offre factice.</em></p>
			  </React.Fragment> },
			{ label: 'Comment scanner un QR code ?',
			  content: <React.Fragment>
					<p>Après avoir lancé une partie au jeu « La Chasse aux Tickets d’Or ». Il suffit au participant d’orienter la caméra de son smartphone vers le QR Code.</p>
					<p>Lors de la détection du QR Code, une animation en réalité apparaît et laisse apparaître le résultat au jeu.</p>
			  </React.Fragment> },
			{ label: 'Combien de fois puis-je jouer à « La Chasse aux Tickets d’Or » ?',
			  content: <React.Fragment>
				  <p>La participation est limitée à une participation par code obtenu durant les dates de Jeu.  Si vous obtenez « X » code(s), vous pourrez réaliser « X » participation(s).</p>
			  </React.Fragment> },
		] },
		{ title: 'Gagnants et dotations jeu « La Chasse aux Tickets d’Or »', questions: [
			{ label: 'Quels sont les gains mis en jeu ?',
			  content: <React.Fragment><p>Lors du jeu « La Chasse aux Tickets d’Or », les gains à remporter sont les suivants :
			  <ul>
				  <li>38.142 lots d’une valeur unitaire de 1€ TTC crédités sur les Cartes U des gagnants</li>
				  <li>25.601 lots d’une valeur unitaire de 3€ TTC crédités sur les Cartes U des gagnants</li>
				  <li>5.661 lots d’une valeur unitaire de 5€ TTC crédités sur les Cartes U des gagnants</li>
				  <li>27 lots d’une valeur unitaire de 250€ TTC sur les Cartes U des gagnants</li>
			  </ul>
		  </p>
		  <p>Voir modalités de participation et détails des dotations dans le règlement de jeu.</p>
			  </React.Fragment> },
			{ label: 'J’ai gagné, comment bénéficier de mes euros carte U ?',
			  content: <React.Fragment><p>Vous avez gagné des € Carte U lors de « La Chasse aux Tickets d’Or » ? Félicitations !</p>
			  <p>Aucune action de votre part n’est requise. L’ensemble de vos gains remportés lors de vos participations au jeu « La Chasse aux Tickets d’Or » seront automatiquement crédités sur votre Carte U, en fin d’opération à partir du Vendredi 07 Décembre 2018.</p>
			  <p>Voir modalités de participation et détails des dotations dans le règlement de jeu.</p>
			  </React.Fragment> },
			{ label: 'J’ai changé de carte U durant le jeu, comment bénéficier de mes gains ?',
			  content: <React.Fragment>
				  <p>Comme mentionné dans le règlement de jeu, dans le cas où des gains ont été remportés sur une Carte U qui aurait été perdue, volée ou endommagée avant la date d’attribution des gains (effectuée le 07/12/2018), l’ensemble de ces gains associés à cette Carte U ne pourront pas être attribué et seront considérés comme perdu. (Voir règlement de jeu).</p>
			  </React.Fragment> },
		] },
		{ title: 'Technique', questions: [
			{ label: 'Je n’ai pas de smartphone, puis-je participer depuis un ordinateur ?',
			  content: <React.Fragment>
				<p>Le jeu nécessitant de scanner des QR Codes dans les rayons des magasins U, la participation doit être effectuée depuis un smartphone compatible.</p>
				<p>Cependant, afin de préparer votre participation en magasin, vous pourrez depuis un ordinateur ou une tablette, sauvegarder vos codes de jeu.</p>
			  </React.Fragment> },
			{ label: 'Quels sont les smartphones compatibles ?',
			  content: <React.Fragment>
				  <p>Afin de garantir une expérience de jeu optimale, la participation au jeu requiert les prérequis techniques suivants:</p>
				<p>Plateforme :
					<ul>
						<li>Smartphone Apple sous la version iOS 11 et supérieure (participation effectuée depuis Safari)</li>
						<li>Smartphone Android sous la version minimale 6.0 et supérieure (participation effectuée depuis Chrome)</li>
						<li>Sur ordinateur le participant pourra uniquement enregistrer un code de participation pour effectuer une Chasse Aux Tickets d’Or dans son magasin U.</li>
					</ul>
				</p>
				<p>Matériel :
					<ul>
						<li>Camera fonctionnelle à l’arrière du smartphone (l'autorisation à l’accès à la caméra afin de scanner des QR Codes devra être acceptée par l’utilisateur).</li>
					</ul>
				</p>
				<p>Connectivité :
					<ul>
						<li>3G minimum</li>
						<li>WIFI</li>
					</ul>
				</p>
			  </React.Fragment> },
		] },
		{ title: 'Connexion, creation de compte', questions: [
			{ label: 'J\'ai oublié mon mot de passe, comment le réinitialiser ?',
			  content: <React.Fragment>
				  <p>Pour réinitialiser votre mot de passe, rendez-vous sur http://www.magasins-u.com.</p>
				<p>Dans l'onglet présent en haut à droite du site, cliquez "Mon espace U" puis sur "Vous avez oublié votre mot de passe ?".</p>
				<p>Saisissez ensuite l’adresse e-mail que vous avez indiquée lors de votre inscription à l’Espace U.</p>
				<p>Un email contenant un lien de réinitialisation vous est alors automatiquement envoyé, cet email comportera un lien vous permettant de renseigner un nouveau mot de passe.</p>
				<p>Attention ce lien est valable 48 heures.</p>
				<p>Note : Les comptes pour accéder à votre Espace magasins-u.com et coursesu.com sont différents, les informations de connexion peuvent donc être différentes.</p>
			  </React.Fragment>},
			{ label: 'Lorsque je souhaite créer mon espace u, un message m\'informe qu\'un compte est déjà associé à cette carte ou que ma date de naissance est incorrecte, que faire ?',
			  content: <React.Fragment>
				  <p>Pour modifier les informations rattachées à votre Carte U telle que votre date de naissance ou votre adresse e-mail, nous vous invitons à :</p>
				<ul>
					<li>Effectuer votre demande avec vos informations à jour via le formulaire de contact magasins-u.com, en <a href="https://www.magasins-u.com/contact" target="_blank">cliquant-ici</a> </li>
					<li>Ou à vous rendre à l'accueil de votre magasin U.</li>
				</ul>
			  </React.Fragment>},
		] },
	]
}
export default class Faq extends ResizeableComponent {
	backLink(config) {
		if(window != top ){
			return (
				<p>
                    <Link to="/" className="back-link">Retour</Link>

				</p>
			)

		}
		else
        	return (
                <a  className="back-link" href={config.loginURL}>Retour</a>
			)
	}
	render(){

		return <div className="faq-page">
			<p className="welcome-message">Questions / Réponses</p>

			{/*<Link to={url} className="back-link">Retour</Link>*/}
			{ this.backLink(config) }
			{ questions.sections.map( section => <React.Fragment>
					<h2>{section.title}</h2>
					<ul className="panel">
						{ section.questions.map( question => <li key={question.label}>
							<QuestionRenderer question={question} />
						</li>) }
					</ul>
				</React.Fragment>)}


			<h2>NOUS N’AVONS PAS RÉPONDU À VOTRE QUESTION ?</h2>
			<ul className="panel">
			<li>
				<div className="other-question">
					<p>Nous vous invitons à nous contacter via le formulaire de contact en <a href="https://www.magasins-u.com/contact" tarbet="_blank">cliquant ici</a></p>
					<p>Pour consulter le règlement du jeu « Le Ticket d’Or », nous vous invitons à consulter la page dédiée à l’opération en <a href="https://www.magasins-u.com/leticketdor" target="_blank">cliquant-ici</a> </p>
					<p>Pour consulter le règlement du jeu « La Chasse aux Tickets d’Or », nous vous invitons à consulter la page dédiée au jeu « La Chasse aux Tickets d’Or » en <a href="https://www.magasins-u.com/leticketdor/jeu-digital" target="_blank">cliquant-ici</a></p>
					<p>Pour toute information relative à la Carte U, nous vous invitons à consulter les conditions générales d’utilisation en <a href="https://www.magasins-u.com/carte_u_conditions_generales_utilisation" target="_blank">cliquant ici</a></p>
				</div>
			</li>
			</ul>
		</div>;
	}
}


