import React from "react";
import { connect } from "react-redux";
import { Route, Switch, withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import config from 'config';
import Modal from "../components/Modal";
import FaqPush from "../components/FaqPush";
import Rules from "../components/Rules";
import Tutorial from '../components/Tutorial';
import Home from './Home';
import DeviceWarning from './DeviceWarning';
import Game from './Game';
import VideoRefused from './VideoRefused';
import GameTutorial from './GameTutorial';
import Faq from './Faq';
import GameSessionStartupConfirmationModal from '../components/GameSessionStartupConfirmationModal';
import GameSessionStartModal from '../components/GameSessionStartModal';
import AttemptResultModal from '../components/AttemptResultModal';
import GameSessionEndModal from '../components/GameSessionEndModal';
import GameCloseConfirmationModal from '../components/GameCloseConfirmationModal';
import GameErrorModal from '../components/GameErrorModal';
import ResizeableComponent from "../utils/ResizableComponent";

class SystemeUTicketDOr extends ResizeableComponent {


    componentDidMount() {
    	localStorage.setItem('windowHref', window.location.search);
	}
	render() {
		const { user } = this.props,
			{ isMobile, isRACompatible, bypassDetection, isIOS, isSafari } = this.props.device,
			deviceWarning= !bypassDetection && isMobile	&& ( !isRACompatible ||(isIOS && !isSafari)
			);
		return (
			<section className="app-container">
				<Switch>
					<Route exact path="/game" component={Game} />
					<Route path="/">
						<div>
							<h1>La chasse aux tickets d'or</h1>
							<Switch>
								<Route exact path="/game-tutorial" component={GameTutorial} />
								<Route exact path="/faq" component={Faq} />
								<Route exact path="/video-refused" >
									<React.Fragment>
										<VideoRefused />
										<FaqPush />
										<Rules />
									</React.Fragment>
								</Route>
								<Route exact path="/" >
									<React.Fragment>
										<p className="welcome-message">{this.getWelcomeMessage()}</p>
										{
											deviceWarning ?
											<DeviceWarning />
											:
											<React.Fragment>
												<Home />
												<Tutorial />
												{ !user && <button className="button inverted login-button" onClick={() => {
                                                    window.gtag('event', 'click_cta', {
                                                        'event_category': 'Home',
                                                        'event_label': 'Login',
                                                        'value': 'Login'
                                                    });
													this.redirectToLoginForm();
                                                }}>
													{
														isMobile ?
														'C\'est parti !' :
														'Renseignez votre code'
													}
												</button> }
											</React.Fragment>
										}
										<FaqPush />
										<Rules />
									</React.Fragment>
								</Route>
							</Switch>
						</div>
					</Route>
				</Switch>
				<Modal
					components={{
						'GAME_SESSION_STARTUP_CONFIRMATION': GameSessionStartupConfirmationModal,
						'GAME_SESSION_START': GameSessionStartModal,
						'ATTEMPT_RESULT': AttemptResultModal,
						'GAME_CLOSE_CONFIRMATION': GameCloseConfirmationModal,
						'GAME_SESSION_END': GameSessionEndModal,
						'GAME_ERROR': GameErrorModal
					}}
				/>
				<div>
                    <a href={config.loginURL} id="goOut" style={{display: 'none'}}>Retour</a>
				</div>
				{ this.props.location.pathname == '/game' && <div className="overlay phone-orientation">
					<div className="modal">
						<div className="content">
						<section>
							<img src={config.assetBaseURL + '/images/icons/phone-orientation.svg'} className="icon" />
							<h4>Ce jeu se joue à la verticale.</h4>
							<br/>
							<p>Veuillez tourner votre smartphone pour continuer.</p>
							<br/>
						</section>
						</div>
					</div>
				</div> }
			</section>
		);
	}

	redirectToLoginForm() {
		window.parent.postMessage( {
			type: 'redirect',
			url: config.loginURL
		}, '*' );
	}

	getWelcomeMessage()
	{
		console.log(this.props.user);
		if ( this.props.user && this.props.user.closedGameSessionsCount > 0 )
		{
			return (
				<div>
					Bon retour {this.props.user.firstname} ! <br />
					en {this.props.user.closedGameSessionsCount} {(this.props.user.closedGameSessionsCount > 1 ? 'parties ' : 'partie ')} vous avez gagné <br />
					<span style={{'font-size': '26px'}}>{this.props.user.totalGainsAmount}€ Carte U !</span>
				</div>
		);
		}
		else if ( this.props.user )
		{
			return <React.Fragment>
				Une nouvelle aventure commence,<br/>bienvenue {this.props.user.firstname} !
			</React.Fragment>;
		}
		else
		{
			return <React.Fragment>
				Partez à l'aventure<br/>et trouvez les Tickets d'Or !
				</React.Fragment>;
		}
	}
}

function mapStateToProps( state )
{
	return {
		user: state.user,
		device: state.device
	};
}

export default withRouter(connect( mapStateToProps )( SystemeUTicketDOr ));