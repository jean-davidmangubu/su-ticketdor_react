import React from 'react';
import config from 'config';
import initARModule from 'initARModule';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'connected-react-router';
import { createGameSessionAttempt } from '../actions/gameSessionAttempt';
import { showModal, hideModal } from '../actions/modal';
import Modal from "../components/Modal";

class Game extends React.Component {
	constructor( ...args ) {
		super( ...args );
		this.start = this.start.bind(this);
		this.hidded = true;
	}

	render() {
		const style = { width: '100%', height: '100%', overflow: 'hidden', position: 'relative'};
		style.display = 'block'; //(this.props.modalVisible) ? 'none' : 'block' ;


		return (
			<div className="game">
				<div id="ar-module" style={style}></div>

                <a href={config.loginURL} id="goOut" style={{display: 'none'}}>Retour</a>
    		</div>
		);
	}

    componentDidUpdate(){

        if(this !== undefined && this.props != undefined && this.props.user.openGameSession !== undefined && this.props.user.openGameSession && (new Date(this.props.user.openGameSession.deadline)) - (new Date()) < 0){
            document.getElementById('goOut').click();
        }
        else {
            if (this.props.modalVisible && this.arModule != null) {
                this.arModule.dispose();
                this.arModule = false;
                console.log('dipose');
                this.hidded = true;

            }
            else if (this.props.modalVisible === false && this.hidded) {
                this.hidded = false;
                console.log('modalvisible');
                if(this.arModule !== undefined && this.arModule.loader != null)
                    this.arModule.dispose();
                this.arModule = this.start.call(this, config);
                if (typeof this.arModule.dispose != 'function') {
                    document.getElementById('goOut').click();
                }
            }
        }

        console.log(this.arModule, 'ar module ?');

    }
    start = (config) => {
        return initARModule({
            element: document.querySelector('div#ar-module'),
            remainingAttempts: this.props.user.openGameSession.remainingAttemptCount,
            remainingTime: (new Date(this.props.user.openGameSession.deadline)) - (new Date()),
            currentGainsAmount: this.props.user.openGameSession.totalGainsAmount,

            assetPath: config.assetBaseURL + '/ar-module/assets',

            onQRCodeScanned: (callback) => {
				this.props.createGameSessionAttempt().then((action) => {
					if (!action.error) {
						const result = this.getAttemptResult();
						console.log('ar-module::callback(', result, ')');
						setTimeout(function () {
							callback(result);
						}, 300);

					}
				});
            },


            onTicketClicked: () => {
                this.props.showModal('ATTEMPT_RESULT', {
                    props: this.getAttemptResult()
                });
            },

            onCloseButtonClicked: () => {
                this.props.showModal('GAME_CLOSE_CONFIRMATION', {});
            },

            onVideoRefused: () => {
                this.props.push('/video-refused');
            },

            modalVisible: () => {

            	return this.props.modalVisible;
            }
        });
    };

	componentDidMount() {

        if( this === undefined || this.props === undefined ){
            //window.location = config.loginURL;
            document.getElementById('goOut').click();
        }
        if(this !== undefined && this.props != undefined && this.props.user.openGameSession !== undefined && this.props.user.openGameSession && (new Date(this.props.user.openGameSession.deadline)) - (new Date()) < 0){
            document.getElementById('goOut').click();
        }

		if(this === undefined || this.props === undefined || this.props.user === undefined || this.props.user.openGameSession === undefined || this.props.user.openGameSession === null){
			//window.location = config.loginURL;
			document.getElementById('goOut').click();
		}


        if((new Date(this.props.user.openGameSession.deadline)) - (new Date()) > 0) {
            this.props.showModal('GAME_SESSION_START', {
                props: this.getAttemptResult()
            });
            var THIS = this;

            setTimeout(function(){
                THIS.props.hideModal('GAME_SESSION_START');

              //  THIS.arModule =  THIS.start.call(THIS, config);

                if(THIS.arModule === null || THIS.arModule === undefined ){

                    document.getElementById('goOut').click();
                }
            }, 4000);


        }
        else
		{

        }

/*
        this.interval = setInterval(() => {


            //this.setState({time: Date.now()});

		}, 500);*/


	}

    _modalVisible(){
        this.setState({time: Date.now()});
        console.log( this.props.modalVisible, 'rand');
        return this.props.modalVisible;
	}

	getAttemptResult() {
		const now = new Date(),
			gameSession = this.props.user.openGameSession || this.props.user.finishedGameSession || {totalGainsAmount:0, remainingAttemptCount: 0, deadline: null},
			deadline = new Date(gameSession.deadline),
			instantWin = this.props.gameSessionAttempt.content ? this.props.gameSessionAttempt.content.instantWin : null;
		return {
			success: !!instantWin,
			gainAmount: instantWin ? instantWin.rewardAmount : 0,
			totalGainsAmount: gameSession.totalGainsAmount,
			remainingAttempts: gameSession.remainingAttemptCount,
			remainingTime: deadline - now
		};
	}

	componentWillUnmount() {
        clearInterval(this.interval);
		/*
		window.parent.postMessage({
			type: 'exitFullscreen'
		}, '*');*/
        try {
        	if(this.arModule != null)
            	this.arModule.dispose();
        } catch(e) {
            console.error(e);
        }
		;
        if(typeof this.props.startGameSession != "undefined")
            this.props.startGameSession().then( () => {

			} );
/*
        if (window != top){

            try
            {
                var oldHref = config.loginURL;
                history.pushState({}, 'Super U', oldHref);
            }
            catch (e) {

            }
            var windowHref = localStorage.getItem('windowHref') || '';

            top.location.href = '/game'+windowHref;


        }*/


	}
	// componentDidUpdate( prevProps ) {
	// 	if ( this.props.user.finishedGameSession && prevProps.user.finishedGameSession !== this.props.user.finishedGameSession )
	// 	{
	// 		this.props.showModal( 'GAME_SESSION_END', {
	// 			props: this.props.user.finishedGameSession
	// 		} );
	// 	}
	// }
}
function mapStateToProps( state )
{
	return {
        windowHref: state.windowHref,
		user: state.user,
		gameSessionAttempt: state.gameSessionAttempt,
		modalVisible: state.modal.visible
	};
}

function mapDispatchToProps( dispatch )
{
	return bindActionCreators( {
		push,
		createGameSessionAttempt,
		showModal,
		hideModal
	}, dispatch );
}

export default connect( mapStateToProps, mapDispatchToProps )( Game );