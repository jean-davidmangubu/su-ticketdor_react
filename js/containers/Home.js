import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'connected-react-router';
import config from 'config';
import { startGameSession } from '../actions/gameSession';
import CodeForm from '../components/CodeForm';
import { showModal, hideModal } from '../actions/modal';
import classNames from 'classnames';

class Home extends React.Component {
	constructor( ...args ) {
		super( ...args );
		this.startGameSession = this.startGameSession.bind( this );
	}

	render() {
		const { user, rewardsState, gameSessionStartup, isMobile } = this.props;
		return (
			<div className="home">
				{
					user
					? <React.Fragment>
						{isMobile && user.openGameSession && this.renderOpenGameSession(user.openGameSession)}
						{!user.openGameSession && user.availableGameSessionCount > 0 && this.renderAvailableGameSessions( user.availableGameSessionCount, isMobile )}
						<section className="code-form">
							<CodeForm
								user={user}
								startGameSession={this.startGameSession}
								gameSessionStartup={gameSessionStartup}
								isMobile={isMobile}
							/>
						</section>
					</React.Fragment>
					: <React.Fragment>
						<p className="intro">
							<img src={config.assetBaseURL + '/images/home/tickets.png'} className="tickets" />
							<br/>
							Avec votre smartphone, scannez les
							QR Codes cachés dans les rayons de votre Magasin U et tentez de gagner
							jusqu’à 250€ Carte U par partie !
						</p>
					</React.Fragment>
				}
			</div>
		);
	}

	renderOpenGameSession( gameSession ) {
		const deadline = new Date( gameSession.deadline );
		const now = new Date();
		const remainingTime = deadline - now;

		const hours = Math.floor( remainingTime / 3600000 );
		const minutes = Math.floor( remainingTime % 3600000 / 60000 );
		return (
			<section className="open-game-session">
				<h4>Attention il vous reste seulement {hours}h{minutes} avant la fin de votre partie !</h4>
				<button
					onClick={() => {
                        window.gtag('event', 'click_cta', {
                            'event_category': 'Home',
                            'event_label': 'Reprenez votre partie !',
                            'value': 'Reprenez votre partie !'
                        });

						this.startGameSession();

                    }}
					disabled={this.props.gameSessionStartup.isLoading}
					className={classNames({
						button: true,
						isLoading: this.props.gameSessionStartup.isLoading
					})}
				>Reprenez votre partie !</button>
			</section>
		)
	}

	renderAvailableGameSessions( count, isMobile )
	{
		return (
			<section className="available-game-sessions">
				<h4>Vous avez {count} {count > 1 ? 'parties' : 'partie'} à jouer !</h4>
				<span>Une partie = 5 QR codes à scanner.</span>
				{
					isMobile && <React.Fragment>
						<br/>
						<button
							onClick={() => {
								window.gtag('event', 'click_cta', {
                                    'event_category': 'Home',
                                        'event_label': 'Jouez',
                                            'value': 'Jouez'
                                                });

								this.startGameSession();
							}}
							disabled={this.props.gameSessionStartup.isLoading}
							className={classNames({
								button: true,
								isLoading: this.props.gameSessionStartup.isLoading
							})}
						>Jouez</button>
					</React.Fragment>
				}
			</section>
		)
	}

	startGameSession()
	{
		const now = new Date(),
			h = now.getHours();
		if ( window.location == 'localhost' || ( h >= 8 && h < 22 ))
		//if(1)
		{
			this.props.push('/game-tutorial');
		}
		else {
			this.props.showModal( 'GAME_SESSION_STARTUP_CONFIRMATION' );
		}
	}
    componentDidMount() {
		this.setState({ windowHref: window.location.href});

		var THIS = this;
        this.interval = setInterval(() => {
        	if(THIS.props.user.openGameSession){
                let deadline = new Date( THIS.props.user.openGameSession.deadline );
                let now = new Date();
                if(deadline.getTime() < now.getTime()){
                    window.location.reload();
                }
			}
            this.setState({time: Date.now()});

        }, 5000);
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }

}

function mapStateToProps( state )
{
	return {
		user: state.user,
		gameSessionStartup: state.gameSessionStartup,
		isMobile: state.device.isMobile
	};
}

function mapDispatchToProps( dispatch )
{
	return bindActionCreators( {
		push,
		showModal,
		hideModal
	}, dispatch );
}

export default connect( mapStateToProps, mapDispatchToProps )( Home );