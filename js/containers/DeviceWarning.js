import React from 'react';
import {connect} from 'react-redux';
import {bypassDeviceDetection} from '../actions/bypassDeviceDetection';

class DeviceWarning extends React.Component {
	render(){
		return <React.Fragment>
			{/* <p className="welcome-message">Partez à l'aventure <br/>et trouvez les Tickets d'Or !</p> */}
			<p className="intro">
				<img src={config.assetBaseURL + '/images/home/tickets.png'} className="tickets" style={{width: 270, marginTop: 20, marginBottom: 30}} />
				<br/>
			</p>
			{this.renderDeviceMessage()}
		</React.Fragment>;
	}

	renderDeviceMessage(){
		const { isRACompatible, unknownRACompatibility, isMobile, isIOS, isSafari} = this.props.device;
		if ( !isRACompatible && unknownRACompatibility ) {
			// impossible de vérifier : android > 4.4 et < 7
			return <React.Fragment>
				<strong>Nous ne sommes pas en meure de<br/> vérifier si votre smartphone<br/>est optimisé pour une expérience<br/>de jeu optimale.</strong>
				<br/>
				<br/>
				<p>Vous pouvez cependant essayer de jouer<br/> en cliquant sur le bouton ci-dessous :</p>
				<button className="button inverted" onClick={()=> {
                    window.gtag('event', 'click_cta', {
                        'event_category': 'Prehome',
                        'event_label': 'Essayer de participer',
                        'value': 'Essayer de participer'
                    });
					this.props.dispatch(bypassDeviceDetection());
                }}>Essayez de participer</button>
			</React.Fragment>;
		} else if ( isIOS && isRACompatible && !isSafari ) {
			// ios compatible RA mais pas safari
			return <React.Fragment>
				<strong>Nous sommes désolés, le navigateur<br/>utilisé n'est pas compatible</strong>
				<br/>
				<br/>
				<p>Afin de vous garantir une expérience de jeu<br/> optimale, nous vous invitons à participer<br/> depuis votre navigateur Safari</p>
				<br/>
			</React.Fragment>;
		}

		// pas compatible : !racomp && !unknow
		return <React.Fragment>
			<strong>Nous sommes désolés, la version de <br/>votre smartphone n'est pas compatible.</strong>
			<br/>
			<br/>
			<p>Pour vous garantir une expérience de jeu<br/>optimale, nous vous invitons à procéder à <br/><strong>la mise à jour de votre smartphone</strong><br/>et renouvelez votre participation.</p>
			<br/>
		</React.Fragment>;
	}
}
function mapStateToProps(state){
	return {
		device: state.device
	}
}
export default connect(mapStateToProps)(DeviceWarning);