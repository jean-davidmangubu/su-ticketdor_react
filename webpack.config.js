const path = require( 'path' );
const webpack = require( 'webpack' );

module.exports = {
	entry: {
		main: './js/app.js',
		iframe: './js/iframe.js'
	},
	output: {
		path: path.resolve(__dirname,'../site/public/js'),
		filename: '[name].js'
	},
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				use: 'babel-loader'
			}
		]
	},
	/*resolve: {
		modules: ["node_modules", path.resolve(__dirname, "./js/kumquats") ]
	},*/
	// resolveLoader: {
	// 	root: path.join(__dirname, 'node_modules')
	// },
	// config générée par symfony
	externals: {
		'config': 'config',
		'initARModule': 'initARModule',
    }
};